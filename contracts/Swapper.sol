//SPDX-License-Identifier: Unlicense
pragma solidity >=0.8.4 <0.9.0;

contract Swapper {

  
  address owner1;
  address owner2;
 
  uint8  public decimals = 5;

  uint256 public totalSupply = 1000000 * (10 ** decimals);

  event Transfer(address indexed from, address indexed to, uint256 value);
  
  constructor(address _owner1, address _owner2) {
    owner1  = _owner1;
    owner2  = _owner2;
    //Tokens
    balanceOf[owner1] = totalSupply;
    balanceOf[owner2] = totalSupply;
  }
  

    // tokens por cada direccion
  mapping(address => uint) public balanceOf;
    

    //Intercambio
  function swap(uint amount1, uint amount2) public {
        require(msg.sender == owner1 || msg.sender == owner2, "Not authorized");
        transferFrom(owner1, owner2, amount1);
        transferFrom(owner2, owner1, amount2);
  }    

  //transferir desde
  function transferFrom(address _from, address _to, uint256 _value) public returns(bool succes){
    require(balanceOf[_from] >= _value, 'insufficient balance');

    balanceOf[_from] -= _value;
    balanceOf[_to]   += _value;
    emit Transfer(_from, _to, _value);
    return true;
  }

  //
  function getBalance(address _owner) public view returns(uint) {
    return balanceOf[_owner];
  }

  // retirada
  function withdraw(uint256 _value) external {
        require(msg.sender == owner1 || msg.sender == owner2, "Not authorized");
        require(balanceOf[msg.sender] >= _value,'insufficient balance');
        uint currentBalance = _value;
        balanceOf[msg.sender] -= currentBalance;
        payable(msg.sender).transfer(currentBalance);
        
  }
  
  fallback() external payable{}
}








