const { expect } = require("chai");
const { ethers } = require("hardhat");
const { smock } =  require('@defi-wonderland/smock');
var chai = require('chai');
chai.should();
chai.use(smock.matchers);

describe("Swapper", function () {
  it("Should return the first balance", async function () {
    const myMockFactory = await smock.mock('Swapper');
    const myMock = await myMockFactory.deploy('0xf39fd6e51aad88f6f4ce6ab8827279cfffb92266','0x70997970c51812dc3a010c7d01b50e0d17dc79c8');
    let balancetest = 100000000000;
    balanceFirstAccount = await myMock.getBalance('0xf39fd6e51aad88f6f4ce6ab8827279cfffb92266');
    expect(balanceFirstAccount.toString()).to.equal(balancetest.toString());
    console.log("balance of the firs account : " + balanceFirstAccount.toString() + " equal balanceTest :" + balancetest);
    
  });

  it("Should return the swap", async function () {
    const myMockFactory = await smock.mock('Swapper');
    const myMock = await myMockFactory.deploy('0xf39fd6e51aad88f6f4ce6ab8827279cfffb92266','0x70997970c51812dc3a010c7d01b50e0d17dc79c8');
    let balancetest = 100000000000;
    const swapp = await myMock.swap(10000,20000);
    const balanceFirstAccount = await myMock.getBalance('0xf39fd6e51aad88f6f4ce6ab8827279cfffb92266');
    const balanceSecondtAccount = await myMock.getBalance('0x70997970c51812dc3a010c7d01b50e0d17dc79c8');
    balancetest = balancetest - 10000 + 20000;
    expect(balancetest.toString()).to.equal(balanceFirstAccount.toString());
    console.log("balance after to from"+" balanceTest : " + balancetest + " " + "balanceFirstAccount "+ balanceFirstAccount);
    console.log("balance after to from"+" balanceTest : " + balancetest + " " + "balanceSecondAccount "+balanceSecondtAccount);
  });


});
